import React, { useState, useEffect } from "react";
import Item from "./Item";
import { useDispatch, useSelector } from "react-redux";
import { addTodo } from "../features/todoSlice";

function Form() {
  const todos = useSelector(state => state.todos)
  const [list, setList] = useState(todos);

  const [task, setTask] = useState("");
  const dispatch = useDispatch()

  

  const [filterState, setFilterState] = useState("all");

  const handleAdd = (e) => { 
    e.preventDefault();
    if (task.trim() != "")
      dispatch(addTodo(task))
      setTask("")
  };

  const filtering = (id) => {
    switch (id) {
      case "all":
        setList(todos);
        setFilterState("all")
        break;
      case "completed":
        const elCompleted = todos.filter((item) => item.completed != false);
        setFilterState("completed")
        setList(elCompleted);

        break;
      case "uncompleted":
        const elUncompleted = todos.filter((item) => item.completed != true);
        setFilterState("uncompleted")
        setList(elUncompleted);
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    if (filterState === "all") {
        setList(todos);
    }
    if (filterState === "completed") {
        const elCompleted = todos.filter((item) => item.completed != false);
        setList(elCompleted);
    }
    if (filterState === "uncompleted") {
        const elUncompleted = todos.filter((item) => item.completed != true);
        setList(elUncompleted);
    }
    
  }, [todos]);

  return (
    <div className="container">
      <div className="mb-5">
        <form action="" className="row gap-3">
          <input
            type="text"
            placeholder="Your To Do"
            name="todo"
            value={task}
            onChange={(e) => setTask(e.target.value)}
          />
          <button
            className="btn btn-rounded btn-grey btn-primary"
            onClick={handleAdd}
          >
            Enregistrer
          </button>
        </form>
      </div>
      <div className="mb-3 d-flex justify-content-lg-between justify-content-md-around justify-content-around">
        <p
          className="text-center border rounded p-3"
          id="all"
          style={{ cursor: "pointer" }}
          onClick={(e) => filtering(e.target.id)}
        >
          Tout
        </p>
        <p
          className="text-center border rounded p-3"
          id="completed"
          style={{ cursor: "pointer" }}
          onClick={(e) => filtering(e.target.id)}
        >
          Achevées
        </p>
        <p
          className="text-center border rounded p-3"
          id="uncompleted"
          style={{ cursor: "pointer" }}
          onClick={(e) => filtering(e.target.id)}
        >
          Inachevées
        </p>
      </div>
      {list.map((todo) => (
        <Item
          id={todo.id}
          todo={todo.todo}
          completed={todo.completed}
          // modifying={modifying}
          // deleting={deleting}
          // isCompleted={isCompleted}
        />
      ))}
    </div>
  );
}

export default Form;
