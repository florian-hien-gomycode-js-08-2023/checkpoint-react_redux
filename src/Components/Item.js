import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteTodo, modifyingTodo, markTodo } from "../features/todoSlice";

function Item({ id, todo, completed, isCompleted }) {
  const [toggleEdition, setToggleEdition] = useState(false);
  const [task, setTask] = useState("");

  const dispatch = useDispatch()

  const handleSubmit = (id, e) => {
    e.preventDefault();
    if (task.trim() != "")
      dispatch(modifyingTodo({id:id, value:task}))
      setToggleEdition(false);
    setTask("");
  };

  const editTaskt = (
    <div className="border border-dark rounded px-md-3 px-1 py-2" id={id}>
      <form action="" method="" className="row gap-3">
        <div className="col-12">
          <p className="fs-5 mb-1">{todo}</p>
          <input
            type="text"
            placeholder="Modif"
            name=""
            className="col-12"
            value={task}
            onChange={(e) => setTask(e.target.value)}
          />
        </div>
        <div className="d-flex col-12 align-items-center justify-content-between">
        <button
          className="col-5 btn btn-rounded btn-primary"
          onClick={ e => handleSubmit(id,e)}
        >
          Modifier
        </button>
        <button
          className="col-5 btn btn-rounded btn-danger"
          onClick={() => setToggleEdition(false)}
        >
          Annuler
        </button>
        </div>
      </form>
    </div>
  );

  const viewTaskt = (
    <div
      className="d-flex align-items-center border border-dark rounded px-md-3 px-1 py-2 my-2"
      id={id}
    >
      <div className="col-2">
        <input
          type="checkbox"
          name=""
          id=""
          checked={completed}
          onChange={(e) => dispatch(markTodo({id:id, value:e.target.checked}))}
        />
      </div>
      <div className="col-8">
        <p className="fs-5 mb-1">{todo}</p>
      </div>
      <div
        className="col-1"
        onClick={() => setToggleEdition(true)}
        style={{ cursor: "pointer" }}
      >
        <img src="/pencil.svg" alt="" style={{ width: 18 }} />
      </div>
      <div
        className="col-1"
        onClick={() => dispatch(deleteTodo(id))}
        style={{ cursor: "pointer" }}
      >
        <img src="/rubbish.svg" alt="" style={{ width: 25 }} />
      </div>

    </div>
  );

  return toggleEdition ? editTaskt : viewTaskt;
}

export default Item;
