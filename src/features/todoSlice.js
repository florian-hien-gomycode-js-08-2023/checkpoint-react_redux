import { createSlice } from '@reduxjs/toolkit'
import { v4 as uuid } from "uuid";

const initialState = {
    todos: [{ id: "1", todo: "Manger", completed: false }, { id: "2", todo: "Manger", completed: false }, { id: "3", todo: "Manger", completed: false }]
}

export const todoSlice = createSlice({
    name: 'todo',
    initialState,
    reducers: {
        addTodo: (state, action) => {
            const item = {
                id: uuid(),
                todo: action.payload,
                completed: false
            }
            state.todos.push(item)
        },
        deleteTodo: (state, action) => {
            state.todos = state.todos.filter(todo => todo.id !== action.payload)
        },
        modifyingTodo: (state, action) => {
            state.todos.map(el => {
                if (el.id === action.payload.id) {
                    el.todo = action.payload.value
                } else {
                    return el
                }
            })
        },
        markTodo: (state, action) => {
            state.todos.map(el => {
                if (el.id === action.payload.id) {
                    el.completed = action.payload.value
                } else {
                    return el
                }
            })
        },
    }
})

export const { addTodo, deleteTodo, modifyingTodo, markTodo } = todoSlice.actions

export default todoSlice.reducer