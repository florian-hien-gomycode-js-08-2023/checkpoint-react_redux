import Form from "./Components/Form";

function App() {
  return (
    <div className="App">
      <div className="container col-lg-3 col-md-7 col-12 bg-light pb-5">
        <h1 className="text-center mb-5">To Do List</h1>
        <Form />
      </div>
    </div>
  );
}

export default App;
